Name:           protobuf-c
Version:        1.3.2
Release:        2
Summary:        This is protobuf-c, a C implementation of the Google Protocol Buffers data serialization format
License:        BSD
URL:            https://github.com/protobuf-c/protobuf-c
Source0:        https://github.com/protobuf-c/protobuf-c/archive/v%{version}.tar.gz 
BuildRequires:  autoconf automake libtool gcc-c++ pkgconfig(protobuf)
Provides:       %{name}-compiler = %{version}-%{release}
Obsoletes:      %{name}-compiler < %{version}-%{release}

%description
This is protobuf-c, a C implementation of the Google Protocol Buffers data serialization format.


%package devel
Summary:        Development files for protobuf-c library
Requires:       %{name} = %{version}-%{release} %{name}-compiler = %{version}-%{release}

%description devel
Protobuf-c-devel contains development files for protobuf-c.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ifv
%configure --disable-static
%make_build

%check
make check

%install
%make_install
%delete_la

%post
/sbin/ldconfig
%postun
/sbin/ldconfig

%files
%doc TODO LICENSE ChangeLog
%{_libdir}/libprotobuf-c.so.*
%{_bindir}/{protoc-c,protoc-gen-c}

%files devel
%dir %{_includedir}/google
%{_includedir}/{google/protobuf-c/,protobuf-c/}
%{_libdir}/{libprotobuf-c.so,pkgconfig/libprotobuf-c.pc}

%changelog
* Fri Feb 14 2020 Senlin Xia <xiasenlin1@huawei.com> - 1.3.2-2
- Package init
